const app = require("express")();
const bodyParser = require('body-parser');
const axios = require('axios');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.get('/', (req, res) => {

    /*axios.get('http://apiself:4000/api')
        .then(function(response) {
            console.log(response.data);
            res.send(response.data);
        })
        .catch(function(error) {
            console.log(error)
            res.json(error);
        })
        .then(function() {
            // always executed
        });*/
    var axios = require('axios');

    var config = {
        method: 'get',
        url: 'https://grcc-testmig.blackboard.com/learn/api/public/v1/courses/_17667010_1/contents/_5864158_1/attachments/_5031597_1/download',
        headers: {
            'Authorization': 'Bearer BD7opZMbDV90zurmzxfKAnmkMtUIWSNF'
        }
    };
    console.log('axios');
    axios(config)
        .then(function(response) {
            res.send(response.data);
        })
        .catch(function(error) {
            console.log(error);
        });

});


app.listen(8000, () => {
    console.log(`App listening at http://localhost:8000`);
});