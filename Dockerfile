FROM node:alpine
RUN mkdir -p /app/web
COPY ./package.json /app/web
WORKDIR /app/web
RUN npm install
COPY . .
CMD ["npm","start"]